﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConstantMovement : MonoBehaviour
{

    public Vector3 movement;
    public bool local = true;

    private Rigidbody2D rb;
    // Use this for initialization
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        var mov = movement * Time.deltaTime;
        if (rb)
        {
            rb.MovePosition(rb.position + (Vector2)(local ? transform.rotation * mov : mov) );
        }
        else
        {
            transform.Translate(mov, local ? Space.Self : Space.World);
        }
    }
}
