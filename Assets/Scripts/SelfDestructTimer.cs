﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelfDestructTimer : MonoBehaviour
{
    public float lifetime = 1f;
    public bool dontDestroyOnLoad = false;

    // Use this for initialization
    IEnumerator Start()
    {
        if (dontDestroyOnLoad) DontDestroyOnLoad(gameObject);

        yield return new WaitForSeconds(lifetime);

        Destroy(gameObject);
    }
}
