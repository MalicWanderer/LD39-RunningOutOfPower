﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHealth : Health
{
    public float regenCooldown = 1;
    public float regenTime = 2;
    public float dieCooldownPenalty = 1;
    public Behaviour[] disableOnDead;
    public Renderer[] disableRenderers;

    private float lastHitTime;

    private void OnCollisionEnter2D(Collision2D collision)
    {
        HandleCollision(collision.collider);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        HandleCollision(collision);
    }

    private void HandleCollision(Collider2D other)
    {
        var damager = other.GetComponent<Damager>();
        if (damager)
        {
            TakeDamage(damager.damage);
            return;
        }

        var enemy = other.GetComponent<Enemy>();
        if (enemy)
        {
            if (enemy.contactDamage > 0)
            {
                TakeDamage(enemy.contactDamage);
            }
        }
    }

    public override void TakeDamage(float damage)
    {
        lastHitTime = Time.time;

        base.TakeDamage(damage);
    }

    private void Update()
    {
        if (health < maxHealth && (Time.time - lastHitTime) > regenCooldown)
        {
            health = Mathf.MoveTowards(health, maxHealth, maxHealth / regenTime * Time.deltaTime);

            if ( health == maxHealth)
            {
                if (disableOnDead != null)
                {
                    foreach (var b in disableOnDead)
                    {
                        b.enabled = true;
                    }
                }
                if (disableRenderers != null)
                {
                    foreach (var r in disableRenderers)
                    {
                        r.enabled = true;
                    }
                }
            }
        }
    }

    protected override void Die()
    {
        if (gibPrefab)
        {
            Instantiate(gibPrefab, transform.position, transform.rotation);
        }

        lastHitTime += dieCooldownPenalty;

        onDied.Invoke();

        if (disableOnDead != null && disableOnDead.Length > 0 || disableRenderers != null && disableRenderers.Length > 0)
        {
            foreach (var b in disableOnDead ?? new Behaviour[0])
            {
                b.enabled = false;
            }
            foreach (var r in disableRenderers ?? new Renderer[0])
            {
                r.enabled = false;
            }
        }
        else
        {
            Destroy(gameObject);
        }
    }
}
