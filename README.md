### Content Generators Used

- Sprite Generator - http://img.uninhabitant.com/spritegen.html
- bfxr - http://www.bfxr.net/
- NeoTextureEdit - http://neotextureedit.sourceforge.net/

### Third Party Code/Libraries

- Cinemachine 2 - https://kharma.unity3d.com/en/#!/content/79898
- Worldspace Shader - https://gamedev.stackexchange.com/questions/111060/unity-tiling-of-a-material-independen-of-its-size