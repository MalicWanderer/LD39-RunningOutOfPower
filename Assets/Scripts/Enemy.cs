﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{

    public float contactDamage = 1;
    public float knockbackTime = .2f;

    private Health health;
    private Rigidbody2D rb;

    private void Start()
    {
        health = GetComponent<Health>();
        rb = GetComponent<Rigidbody2D>();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Damager damager = collision.collider.GetComponent<Damager>();
        if (damager)
        {
            HandleCollision(damager, (transform.position- damager.transform.position).normalized);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Damager damager = collision.GetComponent<Damager>();
        if (damager)
        {
            HandleCollision(damager, (transform.position - damager.transform.position).normalized);
        }
    }

    private void HandleCollision(Damager damager, Vector2 kbDir)
    {
        if (damager.damage > 0)
        {
            health.TakeDamage(damager.damage);
        }
        if (damager.knockback > 0)
        {
            var kb = kbDir * damager.knockback;
            StartCoroutine(KnockbackRoutiine(kb));
        }
    }

    private IEnumerator KnockbackRoutiine(Vector2 kb)
    {
        yield return null;

        var perSecondKb = kb / knockbackTime;
        var t = 0f;

        while (t < knockbackTime)
        {
            rb.MovePosition(rb.position + perSecondKb * Time.deltaTime);
            t += Time.deltaTime;
            yield return null;
        }
    }

}
