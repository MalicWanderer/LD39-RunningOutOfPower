﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PersistentData : MonoBehaviour
{

    public SceneField nextLevel;
    public bool hasGrip = true, hasSupercharge = true, hasPowerfist = true, hasShield = true;

    // Use this for initialization
    void Start()
    {
        DontDestroyOnLoad(gameObject);
    }

}
