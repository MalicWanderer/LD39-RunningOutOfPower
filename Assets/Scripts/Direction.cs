﻿using UnityEngine;
using System.Collections;

[System.Flags]
public enum Direction
{
    None = 0,
    Up = 1 << 0,
    Down = 1 << 1,
    Right = 1 << 2,
    Left = 1 << 3,
    UpRight = Up | Right,
    UpLeft = Up | Left,
    DownRight = Down | Right,
    DownLeft = Down | Left,
}

public static class DirectionExtensions
{
    private const Direction UpDown = Direction.Up | Direction.Down;
    private const Direction RightLeft = Direction.Right | Direction.Left;

    public static Direction Sanitize(this Direction dir, bool preferDown = false, bool preferLeft = false)
    {
        var res = dir;
        if ((dir & UpDown) == UpDown)
        {
            if (preferDown)
            {
                res &= ~Direction.Up;
            }
            else
            {
                res &= ~Direction.Down;
            }
        }
        if ((dir & RightLeft) == RightLeft)
        {
            if (preferLeft)
            {
                res &= ~Direction.Right;
            }
            else
            {
                res &= ~Direction.Left;
            }
        }

        return res;
    }
}