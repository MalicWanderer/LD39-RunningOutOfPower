﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInput : MonoBehaviour
{

    public string leftRightAxis = "Horizontal";
    public string upDownAxis = "Vertical";
    public string shootButton = "Fire1";
    public string punchButton = "Fire2";

    public Shooter gunShooter;
    public Shooter punchShooter;

    private MoveAndTurn mover;

    // Use this for initialization
    void Start()
    {
        mover = GetComponent<MoveAndTurn>();
        gunShooter = GetComponent<Shooter>();
    }

    // Update is called once per frame
    void Update()
    {
        var hor = Input.GetAxisRaw(leftRightAxis);
        if (hor < -.01)
        {
            mover.Left = true;
            mover.Right = false;
        }
        else if (hor > .01)
        {
            mover.Right = true;
            mover.Left = false;
        }
        else
        {
            mover.Left = mover.Right = false;
        }

        var vert = Input.GetAxisRaw(upDownAxis);
        if (vert < -.01)
        {
            mover.Down = true;
            mover.Up = false;
        }
        else if (vert > .01)
        {
            mover.Up = true;
            mover.Down = false;
        }
        else
        {
            mover.Down = mover.Up = false;
        }

        if (Input.GetButton(shootButton))
        {
            gunShooter.TryShoot();
        }
        if (Input.GetButton(punchButton))
        {
            punchShooter.TryShoot(); 
        }
    }
}
