﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

    private PersistentData data;

    public bool HasShield { get { return data.hasShield; } set { data.hasShield = value; } }
    public bool HasGrip { get { return data.hasGrip; } set { data.hasGrip = value; } }
    public bool HasSupercharge { get { return data.hasSupercharge; } set { data.hasSupercharge = value; } }
    public bool HasPowerfist { get { return data.hasPowerfist; } set { data.hasPowerfist = value; } }
    public SceneField NextLevel { get { return data.nextLevel; } set { data.nextLevel = value; } }

    private void Awake()
    {
        data = FindObjectOfType<PersistentData>();
        if (data == null)
        {
            data = (new GameObject("Data")).AddComponent<PersistentData>();
        }
    }

    public void GoToNextLevel()
    {
        SceneManager.LoadScene(NextLevel.Name);
    }
}
