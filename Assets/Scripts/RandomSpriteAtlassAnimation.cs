﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.U2D;

public class RandomSpriteAtlassAnimation : MonoBehaviour
{
    public SpriteAtlas atlas;
    public float minChangeTime = .1f;
    public float maxChangeTime = .3f;

    private SpriteRenderer ren;

    // Use this for initialization
    IEnumerator Start()
    {
        ren = GetComponent<SpriteRenderer>();
        var sprites = new Sprite[atlas.spriteCount];
        atlas.GetSprites(sprites);

        while (true)
        {
            ren.sprite = sprites[Random.Range(0, sprites.Length)];
            yield return new WaitForSeconds(Random.Range(minChangeTime, maxChangeTime));
        }
    }
    
}
