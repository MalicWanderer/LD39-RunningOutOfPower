﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CinemachineFollowPlayer : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {
        GetComponent<Cinemachine.CinemachineVirtualCameraBase>().Follow = GameObject.FindGameObjectWithTag("Player").transform;
    }
    
}
