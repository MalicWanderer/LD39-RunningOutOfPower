﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GradientImageAnimation : MonoBehaviour
{
    public Gradient gradient;
    public float speed = 1;

    private Image image;
    private float currentValue;

    // Use this for initialization
    void Start()
    {
        image = GetComponent<Image>();
    }

    // Update is called once per frame
    void Update()
    {
        currentValue += speed * Time.deltaTime;
        image.color = gradient.Evaluate(Mathf.PingPong(currentValue, 1));
    }
}
