﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnableBehaviourOnVisible : MonoBehaviour
{
    public Behaviour[] targets;

    private void OnBecameVisible()
    {
        foreach (var t in targets)
        {
            t.enabled = true;
        }
    }
}
