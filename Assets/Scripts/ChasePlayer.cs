﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChasePlayer : MonoBehaviour
{

    public float speed = 3;
    public float turnSpeed = 60;
    public bool moveDirect;
    public float stopDistance = .2f;
    [Range(0, 180)]
    public float maxMoveAngle = 20;

    private static Transform player;
    private Rigidbody2D rb;

    // Use this for initialization
    void Start()
    {
        if (!player) player = GameObject.FindGameObjectWithTag("Player").transform;

        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        var target = player.position;
        var dir = target - transform.position;

        var angle = Vector2.SignedAngle(Vector2.up, dir);

        if (turnSpeed > 0)
        {
            if (rb)
            {
                rb.MoveRotation(Mathf.MoveTowardsAngle(rb.rotation, angle, turnSpeed * Time.deltaTime));
            }
            else
            {
                var rot = Quaternion.RotateTowards(transform.rotation, Quaternion.Euler(0, 0, angle), turnSpeed * Time.deltaTime);
                transform.rotation = rot;
            }
        }

        if (maxMoveAngle < 180)
        {
            var faceAngle = Vector2.Angle(transform.up, dir);
            if (faceAngle > maxMoveAngle)
            {
                return;
            }
        }

        if (stopDistance > 0)
        {
            if (dir.sqrMagnitude <= stopDistance * stopDistance)
            {
                return;
            }
        }

        Vector2 moveTo;
        if (moveDirect)
        {
            moveTo = Vector2.MoveTowards(transform.position, target, speed * Time.deltaTime);   
        }
        else
        {
            moveTo = transform.position + transform.up * speed * Time.deltaTime;
        }

        if (rb)
        {
            rb.MovePosition(moveTo);
        }
        else
        {
            transform.position = moveTo;
        }
    }
}
