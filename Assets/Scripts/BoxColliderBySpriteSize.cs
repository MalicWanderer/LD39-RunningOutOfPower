﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxColliderBySpriteSize : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {
        GetComponent<BoxCollider2D>().size = GetComponent<SpriteRenderer>().size;
    }
}
