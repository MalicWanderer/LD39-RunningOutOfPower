﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneChanger : MonoBehaviour
{
    public SceneField scene;
    public SceneField nextLevel;

    public void ChangeScene()
    {
        if (nextLevel.IsSet)
        {
            FindObjectOfType<GameManager>().NextLevel = nextLevel;
        }
        SceneManager.LoadScene(scene.Name);
    }
}
