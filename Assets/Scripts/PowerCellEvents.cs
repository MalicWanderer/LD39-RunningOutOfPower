﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PowerCellEvents : MonoBehaviour
{

    public UnityEvent noFist, noGrip, noShield, noSupercharge;

    // Use this for initialization
    void Start()
    {
        var data = FindObjectOfType<PersistentData>();
        if (!data.hasPowerfist)
        {
            noFist.Invoke();
        }
        if (!data.hasGrip)
        {
            noGrip.Invoke();
        }
        if (!data.hasShield)
        {
            noShield.Invoke();
        }
        if (!data.hasSupercharge)
        {
            noSupercharge.Invoke();
        }
    }
}
