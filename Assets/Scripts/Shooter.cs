﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooter : MonoBehaviour
{
    [SerializeField]
    private GameObject projectilePrefab;
    public GameObject ProjectilePrefab { get { return projectilePrefab; } set { projectilePrefab = value; } }

    public Transform barrel;

    [SerializeField]
    private float cooldownTime = .2f;
    public float CooldownTime { get { return cooldownTime; } set { cooldownTime = value; } }
    [SerializeField]
    private float maxScatterStrength = 15;
    public float MaxScatterStrength { get { return maxScatterStrength; } set { maxScatterStrength = value; } }
    [SerializeField]
    private float scatterStrengthPerShot = 2;
    public float ScatterStrengthPerShot { get { return scatterStrengthPerShot; } set { scatterStrengthPerShot = value; } }
    [SerializeField]
    private float scatterRecoverOneShotTime = .4f;
    public float ScatterRecoverOneShotTime { get { return scatterRecoverOneShotTime; } set { scatterRecoverOneShotTime = value; } }

    [Header("Animation")]
    public string triggerName = "Shoot";

    private Animator anim;
    private float lastShotTime;
    private float noiseStrength;

    private void Start()
    {
        anim = GetComponent<Animator>();
    }

    public void TryShoot()
    {
        if (enabled && Time.time - lastShotTime > cooldownTime)
        {
            Shoot();
        }
    }

    private void Shoot()
    {
        var dt = Time.time - lastShotTime;
        noiseStrength = Mathf.Max(0, noiseStrength - dt * scatterStrengthPerShot / scatterRecoverOneShotTime);

        var rot = barrel.rotation;
        if (noiseStrength > 0)
        {
            rot = rot * Quaternion.Euler(0, 0, Random.Range(-noiseStrength, noiseStrength));
        }

        Instantiate(projectilePrefab, barrel.position, rot);
        lastShotTime = Time.time;

        if (anim) anim.SetTrigger(triggerName);

        noiseStrength = Mathf.Min(noiseStrength + scatterStrengthPerShot, maxScatterStrength);
    }
}