﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthTrackImage : MonoBehaviour
{
    public Health target;
    public Gradient gradient;
    public bool hideWhen0;

    private Image image;

    private void Start()
    {
        image = GetComponent<Image>();
    }

    // Update is called once per frame
    void Update()
    {
        image.color = gradient.Evaluate(target.health / target.maxHealth);
        if (hideWhen0 && (target.health <= 0 || !target.gameObject.activeSelf))
        {
            image.color = Color.clear;
        }
    }
}
