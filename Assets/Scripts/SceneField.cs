using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

[System.Serializable]
public struct SceneField
{
#pragma warning disable 0649
    [SerializeField]
    private string path;
#pragma warning restore 0649

    /// <summary>
    /// Proper value for passing into <see cref="UnityEngine.SceneManagement.SceneManager.LoadScene(string)"/> and similar.
    /// </summary>
    public string Name
    {
        get
        {
            return path.Remove(path.Length - ".unity".Length).Substring("Assets/".Length);
        }
    }

    /// <summary>
    /// Should match <see cref="UnityEngine.SceneManagement.Scene.path"/>.
    /// </summary>
    public string FullPath
    {
        get { return path; }
    }

    /// <summary>
    /// Is there an assigned Scene?
    /// </summary>
    public bool IsSet
    {
        get { return !string.IsNullOrEmpty(path); }
    }
}