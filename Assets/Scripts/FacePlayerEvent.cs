﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class FacePlayerEvent : MonoBehaviour
{
    public float triggerArc = 10;

    public UnityEvent startFacingPlayer, continueFacingPlayer, endFacingPlayer;

    private Transform player;

    public bool isFacingPlayer { get; private set; }

    void Start()
    {
        if (!player) player = GameObject.FindGameObjectWithTag("Player").transform;
    }

    // Update is called once per frame
    void Update()
    {
        var dir = player.position - transform.position;
        var faceAngle = Vector2.Angle(transform.up, dir);
        if (faceAngle <= triggerArc)
        {
            if (isFacingPlayer)
            {
                continueFacingPlayer.Invoke();
            }
            else
            {
                isFacingPlayer = true;
                startFacingPlayer.Invoke();
            }
        }
        else if (isFacingPlayer)
        {
            isFacingPlayer = false;
            endFacingPlayer.Invoke();
        }
    }
}
