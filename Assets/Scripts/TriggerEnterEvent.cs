﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TriggerEnterEvent : MonoBehaviour
{
    public UnityEvent triggerEntered;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        triggerEntered.Invoke();
    }
}
