﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveAndTurn : MonoBehaviour
{

    public float speed = 5;

    [Header("Animation")]
    public string moveParameter = "Speed";
    public float animSpeedParamValue = 1;

    private Animator anim;

    public Direction Direction { get; set; }
    public bool Up
    {
        get { return (Direction & Direction.Up) == Direction.Up; }
        set
        {
            if (value) Direction |= Direction.Up;
            else Direction &= ~Direction.Up;
        }
    }
    public bool Down
    {
        get { return (Direction & Direction.Down) == Direction.Down; }
        set
        {
            if (value) Direction |= Direction.Down;
            else Direction &= ~Direction.Down;
        }
    }
    public bool Right
    {
        get { return (Direction & Direction.Right) == Direction.Right; }
        set
        {
            if (value) Direction |= Direction.Right;
            else Direction &= ~Direction.Right;
        }
    }
    public bool Left
    {
        get { return (Direction & Direction.Left) == Direction.Left; }
        set
        {
            if (value) Direction |= Direction.Left;
            else Direction &= ~Direction.Left;
        }
    }

    // Use this for initialization
    void Start()
    {
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 move;
        Quaternion rot;
        bool animate = true;

        switch (Direction.Sanitize())
        {
            case Direction.Up:
                rot = Quaternion.identity;
                move = Vector3.up;
                break;
            case Direction.Down:
                rot = Quaternion.Euler(0, 0, 180);
                move = Vector3.down;
                break;
            case Direction.Right:
                rot = Quaternion.Euler(0, 0, -90);
                move = Vector3.right;
                break;
            case Direction.Left:
                rot = Quaternion.Euler(0, 0, 90);
                move = Vector3.left;
                break;
            case Direction.UpLeft:
                rot = Quaternion.Euler(0, 0, 45);
                move = Vector3.up + Vector3.left;
                move.Normalize();
                break;
            case Direction.UpRight:
                rot = Quaternion.Euler(0, 0, -45);
                move = Vector3.up + Vector3.right;
                move.Normalize();
                break;
            case Direction.DownLeft:
                rot = Quaternion.Euler(0, 0, 135);
                move = Vector3.down + Vector3.left;
                move.Normalize();
                break;
            case Direction.DownRight:
                rot = Quaternion.Euler(0, 0, -135);
                move = Vector3.down + Vector3.right;
                break;
            default:
                rot = transform.rotation;
                move = Vector3.zero;
                animate = false;
                break;
        }

        transform.position += move * speed * Time.deltaTime;
        transform.rotation = rot;

        if (anim) anim.SetFloat(moveParameter, animate ? animSpeedParamValue : 0);

    }
}