﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Health : MonoBehaviour
{

    public float maxHealth = 1;
    public bool flashOnDamage = true;
    public bool soundOnDamage = false;
    public Transform gibPrefab;

    public UnityEvent onDied;

    public float health { get; protected set; }
    private SpriteFlasher flasher;
    new private AudioSource audio;

    // Use this for initialization
    void Start()
    {
        health = maxHealth;
        flasher = GetComponent<SpriteFlasher>();
        audio = GetComponent<AudioSource>();
    }

    public virtual void TakeDamage(float damage)
    {
        health -= damage;

        if (flashOnDamage && flasher)
        {
            flasher.Flash();
        }

        if (soundOnDamage && audio)
        {
            audio.Play();
        }

        if (health <= 0)
        {
            Die();
        }
    }

    protected virtual void Die()
    {
        if (gibPrefab)
        {
            Instantiate(gibPrefab, transform.position, transform.rotation);
        }

        Destroy(gameObject);

        onDied.Invoke();
    }
}
