﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class SpriteFlasher : MonoBehaviour
{

    //public bool flashSelf;
    //public string flashChildRegex = ".*_Flash$";

    public float flashTime = .25f;
    public float repetitions = 1;
    public float offTime = .25f;
    public bool interrupt = false;

    public SpriteRenderer[] flashRenderers;
    private Coroutine currentRoutine;

    public void Flash()
    {
        if (interrupt)
        {
            StopRoutine();
        }
        else if (currentRoutine != null)
        {
            currentRoutine = StartCoroutine(WaitRoutine(currentRoutine));
            return;
        }

        currentRoutine = StartCoroutine(FlashRoutine());
    }

    public void StopFlash()
    {
        StopRoutine();
        foreach (var r in flashRenderers)
        {
            r.gameObject.SetActive(false);
        }
    }

    private IEnumerator WaitRoutine(Coroutine current)
    {
        yield return current;
        yield return StartCoroutine(FlashRoutine());
    }

    private void StopRoutine()
    {
        if (currentRoutine != null)
        {
            StopCoroutine(currentRoutine);
            currentRoutine = null;
        }
    }

    private IEnumerator FlashRoutine()
    {
        WaitForSeconds flashWait = new WaitForSeconds(flashTime);
        WaitForSeconds offWait = new WaitForSeconds(offTime);

        for (int i = 0; i < repetitions; i++)
        { 
            foreach (var r in flashRenderers)
            {
                r.gameObject.SetActive(true);
            }

            yield return flashWait;

            foreach (var r in flashRenderers)
            {
                r.gameObject.SetActive(false);
            }

            yield return offWait;
        }
    }
}
